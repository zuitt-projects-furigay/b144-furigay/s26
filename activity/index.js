const http = require('http');
const port = 3000

const server = http.createServer((request, response) => {

	if(request.url === '/login'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('You are in the login page')
	}else {
		response.writeHead(200,{'Content-Type': 'text/plain'});
		response.end('Page not found Status 404')
	}

})


server.listen(port);

console.log(`Server now accesible at localhost: ${port}`)