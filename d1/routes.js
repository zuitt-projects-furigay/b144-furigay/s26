const  http = require('http');

// create a variable "port" to store the port number.
const port = 4000

// Create a variable 'server' that stores the output of the 'create server' method
const server = http.createServer((req, res) => {
	// http://localhost:4000/greeting
	if(req.url == '/greeting'){
		res.writeHead(200,{'Content-Type': 'text/plain'});
		res.end('Hello Again')
	}
	// Access 
	else if(req.url == '/homepage'){
		res.writeHead(200,{'Content-Type': 'text/plain'})
		res.end('This is the homepage')
	}
	// All other routes will return a message "Page not found" with a status of 404
	else {
			res.writeHead(200, {'Content-Type': 'text/plain'})
			res.end('Page not found Status 404')
		}			


})

// Use the 'server ' annd 'port' variables creates above
server.listen(port);

// When server is running, console will print the message:
console.log(`Server now accesible at localhost: ${port}`)

