/*
	require diirective
		
		to load Node.js Modules

	Modules

		is a software components or part of a program that contains one or more routines.

	HTTP 

		Hyper Text Transfer Protocol 

	'http module'

		lets node.js


*/
let http = require("http");
// Clients (browser) and servers (nodeJs/ExpressJs application) communicate by exchanging individual message.
// message sent by the client, usually a web bbrowseer are called requests.
// http://home
// message sent by the server as an answer are called responses.

// createServer() Method - used to create a HTTP server that listens to a request on a specifiedd port and gives responses back to the client.
// It accepts a function and allows us to perform a certian task for our server.
http.createServer(function(request, response) {

	// Use the writeHead() method : 
	// to set a status code for the response 
	// Set the content-type of the response as a plain text message
	response.writeHead(200,{'Content-Type': 'text/plain'});
	// We used the response.end() method to end the response process.
	response.end("GoodBye")

}).listen(4000)
// A port is a virtual point where network connection start and end.
// each port is associated a specific process or service.
// The server will be assignned a to port 4000 via the "listen(4000)" method where the server will listen to any request that are sent to our server.

// When a server is running, console will print this message.
console.log('Server running at localhost:4000')

// http://localhost:4000


// --------------------------------------------------------------

